import {registerRoot} from 'remotion';
import {RemotionVideo} from './components/Video';

registerRoot(RemotionVideo);
