import { useEffect, useState } from "react";
import { IFragment, IInput, InputType } from "../interfaces";
import { useVideoConfig, delayRender, continueRender } from "remotion";
const inputs: IInput[] = require('../../input.json'); 

export const useFragments = () => {
  const [fragments, setFragments] = useState<IFragment[]>([]);
  const videoConfig = useVideoConfig();

  let from = 0;
  const addFragment = (input: IInput) => {
    const duration = videoConfig.fps * input.durationInSeconds;
    const fragment: IFragment = {
      ...input,
      from,
      duration,
    };
    
    setFragments(prevState => [...prevState, fragment]);
    from += duration;
  }

  useEffect(() => {
    inputs.forEach(addFragment);
  }, []);

  return {
    fragments,
  }
}
