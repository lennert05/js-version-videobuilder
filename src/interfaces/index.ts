export type IInput = IImageAudio | ISlide;
export type IFragment = IImageAudioFragment | ISlideFragment;

export interface IImageAudio {
  id: number,
  audio: string,
  image: string,
  type: InputType,
  durationInSeconds: number,
  title: string
}

export interface ISlide {
  id: number,
  text: string,
  type: InputType,
  durationInSeconds: number
}

export interface IImageAudioFragment extends IImageAudio {
  duration: number,
  from: number,
}

export interface ISlideFragment extends ISlide {
  duration: number,
  from: number,
}

export enum InputType {
  SLIDE = "SLIDE",
  IMAGE_AUDIO = "IMAGE_AUDIO",
}