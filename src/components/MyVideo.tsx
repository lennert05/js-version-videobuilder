import { ImageAudio } from "./ImageAudio";
import { Sequence } from "remotion";
import { Slide } from "./Slide";
import { useFragments } from "../hooks/useFragments";
import { IFragment, IInput, InputType } from "../interfaces";

export const MyVideo = () => {
  const { fragments } = useFragments();

  return (
    <div>
      {fragments.map((fragment: IFragment) => {
          if (fragment.type === InputType.IMAGE_AUDIO) {
            // @ts-ignore
            return (<ImageAudio key={fragment.id} from={fragment.from} duration={fragment.duration} image={fragment.image} audio={fragment.audio} />)
          }
          // @ts-ignore
          return (<Slide key={fragment.id} from={fragment.from} duration={fragment.duration} text={fragment.text} />);
          
      })}
    </div>
  )
}