import { useEffect, useState } from 'react';
import {Composition, useVideoConfig} from 'remotion';
import { IInput } from '../interfaces';
import { MyVideo } from './MyVideo';
const inputs: IInput[] = require('../../input.json'); 

export const RemotionVideo: React.FC = () => {
	const [totalDuration, setTotalDuration] = useState(1);

	const getTotalDuration = (): number => {
    const total = inputs.reduce((acc: number, input: IInput) => acc + input.durationInSeconds, 0);
    return 30 * total;
  }

	useEffect(() => {
		setTotalDuration(getTotalDuration);
	}, []);

	return (
		<>
			<Composition
				id="MyVideo"
				component={MyVideo}
				durationInFrames={totalDuration}
				fps={30}
				width={1920}
				height={1080}
			/>
		</>
	);
};
