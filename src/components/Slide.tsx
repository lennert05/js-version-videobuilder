import { Sequence } from "remotion"

interface IProps {
  from: number,
  duration: number,
  text: string
}

export const Slide: React.FC<IProps> = (props: IProps) => {
  const { from, duration, text } = props;

  return (
    <div>
      <Sequence from={from} durationInFrames={duration}>
        <h1 style={{margin: "auto", fontSize: "4rem"}}>{text}</h1>
      </Sequence>
    </div>
  )
}