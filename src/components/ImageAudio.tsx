import React, { lazy, useEffect, useState } from "react";
import { Img, Audio, Sequence, continueRender, delayRender } from "remotion";

interface IProps {
  image: string,
  audio: string,
  from: number,
  duration: number,
  title: string
}

export const ImageAudio: React.FC<IProps> = (props) => { 
  const { audio, image, from, duration, title } = props;

  const [imageFile, setImageFile] = useState("");
  const [audioFile, setAudioFile] = useState("");
  

  useEffect(() => {
    // @ts-ignore
    const context = require.context('/home/wannes/videos-content', true);
    const imageFile = context(`./${image}`);
    const audioFile = context(`./${audio}`);

    if (audioFile && imageFile) {
      setImageFile(imageFile);
      setAudioFile(audioFile);
    }
  }, [image, audio]);

  return (
      <div>
        { imageFile && audioFile ?
          <Sequence from={from} durationInFrames={duration}>
            <div style={{width: "100%"}}>
              <h1 style={{width:"100%", textAlign: "center", fontSize: "3rem"}}>{title}</h1>
              <div style={{display: "grid", placeItems: "center", justifyContent: "center", width: "100%", height: "100%"}}>
                <Audio src={audioFile} />
                <Img src={imageFile} style={{objectFit: "contain" }}/>
              </div>
            </div>
          </Sequence>
        : null }
      </div>
  );
}