# Project V - Video Content Generator
Een programma om een video te maken van een beurs rapport.

Dit is het video rendering programma, het leest een JSON input bestand, die gestuurd wordt vanuit de java server en zet het om in een video die je kan bekijken via de webapplicatie of kan builden in een video. Dit was ontwikkelt met Remotion.dev, een tool geschreven in React om video's te genereren met HTML en CSS. Achter de schermen maakt dit gebruik van FFMpeg.

![Videobuilder Web Interface](https://i.imgur.com/mGP6KmJ.png)


## Inhoudstabel
* [Opzetten](#opzetten)
   + [Voorbereiding](#voorbereiding)
   + [Installatie](#installatie)
* [Uitleg Files](#uitleg-files)
   + [/src/components/Video](#-src-components-video)
   + [/src/components/MyVideo](#-src-components-myvideo)
   + [/src/components/Slide](#-src-components-slide)
   + [/src/components/ImageAudio](#-src-components-imageaudio)
   + [/src/interfaces/index](#-src-interfaces-index)
   + [/src/hooks/useFragment](#-src-hooks-usefragment)
* [Auteurs](#authors)

## Opzetten
Om een lokale kopie op te zetten, volg de onderstaande stappen.

### Voorbereiding
* node v16.x.x
* npm
  ```sh
  npm install npm@latest -g
  ```
* yarn
  ```sh
  npm install --global yarn
  ```
  
### Installatie
1. Clone de repository
   ```sh
   git clone git@git.ti.howest.be:se-project/js-version-videobuilder.git
   ```
2. Ga naar de JS-version-videobuilder folder
   ```sh
   cd JS-version-videobuilder
   ```
3. Installeer de dependencies
   ```sh
   yarn
   ```
4. Zet je juiste output folder voor de video in package.json, build script
   ```
   "build": "remotion render src/index.tsx MyVideo out/video.mp4",
   ```
5. Zet je image en video path in /src/components/ImageAudio.tsx, regel 21
   ```
   const context = require.context('/home/wannes/videos-content', true);
   ```
6. Run de web interface voor testing
   ```
   yarn start
   ```
6. OF build de video
   ```
   yarn build
   ```

## Uitleg Files

### /src/components/Video
In deze file gaan we de totale duur van de video gaan berekenen en geven we die meteen mee aan een composition. Een composition is eigenlijk een component om een video te gaan renderen. Stel je voor, je wil een video maken van een technische analyse en een fundamentele analyse en deze hebben allebei een verschillende opmaak: dan maken we een compositie voor Fundamental en Technical.

Dan kunnen we kiezen welke video we runnen door:
   ```
   remotion render src/index.tsx Fundamental out/fundamental.mp4
   remotion render src/index.tsx Technical out/technical.mp4
   ```

### /src/components/MyVideo
Dit is de component die werd meegegeven in onze composition. Hierin gaan we alle fragmenten die werden opgemaakt door de hook Fragments gaan mappen naar een component van Slide of Images. We geven hieraan een duration mee, een from: vanaf wanneer de slide moet afgespeeld worden en de bijhorende properties van het type Fragment.

### /src/components/Slide
De opmaak van een Slide fragment: bevat een from, duration en een titel.

### /src/components/ImageAudio
De opmaak van een ImageAudio fragment: speelt een audio file af met een bijbehorende afbeelding die wordt getoond. Dit bevat een duration, from, audio en een image. In de functie useEffect worden de images en audios geïmporteerdt, want enkel als path kan je ze niet meegeven. Normaal gezien zou je vanboven zeggen "import image from path", maar aangezien we de files niet op voorhand weten, is dit de dynamische manier.

### /src/hooks/useFragment
Hierin wordt berekend wanneer de video moet beginnen met afspelen, door de durations op te tellen en een fragment te starten vanaf de vorige video afgespeeld is.

### /src/interfaces/index
Alle typescript interfaces

## Auteurs
* **Lennert Commeine**
* **Wannes Matthys**
